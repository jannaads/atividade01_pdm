package com.example.atividade_01;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class Tarefas extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tarefas);
	}

	
	public void voltar(View view) {
    	Intent intent = new Intent(Tarefas.this, Dashboard.class);
    	Tarefas.this.startActivity(intent);
    }
}
