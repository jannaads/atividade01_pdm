package com.example.atividade_01;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Dashboard extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dashboard);
	}

	public void clicar(View view) {
		Intent it = new Intent(Dashboard.this, Tarefas.class);
		startActivity(it);
	}
}
